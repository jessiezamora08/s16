// while loop - habang ang condition ay namimeet or nasasatisfy 

let count = 1;
while (count < 11) {
    console.log ("count is " + count)
    count++ //increment operator 
}

// let num = 1;
// while (num <= 10) {
// console.log (num)
// num += 2;
// }

// let num = -10;
// while (num < 20) {
// console.log (num)
// num ++
// }

let num = 10;
while (num < 41) {
    console.log (num)
    num += 2;
}

let x = 301;
while (x < 334) {
    console.log (x)
    x +=2;
}
